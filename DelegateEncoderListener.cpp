#include "DelegateEncoderListener.h"


void DelegateEncoderListener::onEncoderClick() {
	EncoderListener* delegateListener = getDelegateListener();
	if (delegateListener != nullptr) {
		delegateListener->onEncoderClick();
	}
}

void DelegateEncoderListener::onEncoderOffset(int8_t offset) {
	EncoderListener* delegateListener = getDelegateListener();
	if (delegateListener != nullptr) {
		delegateListener->onEncoderOffset(offset);
	}
}
