#ifndef DELEGATEENCODERLISTENER_H
#define DELEGATEENCODERLISTENER_H

#include <EncoderListener.h>

class DelegateEncoderListener: public EncoderListener {
	
protected:

	virtual EncoderListener* getDelegateListener() = 0;

    
public:

	virtual void onEncoderClick() override;
	virtual void onEncoderOffset(int8_t offset) override;
	
	
};

#endif //DELEGATEENCODERLISTENER_H
