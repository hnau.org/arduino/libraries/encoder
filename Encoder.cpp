#include "Encoder.h"


const PROGMEM int8_t Encoder::offsetMap[16] = 
	{0, -1, 1, 0, 1, 0, 0, -1, -1, 0, 0, 1, 0, 1, -1, 0};

Encoder::Encoder(
	uint8_t pinS1,
	uint8_t pinS2,
	uint8_t pinKey,
	EncoderListener& listener_
): listener(listener_) {
	this->pinS1 = pinS1;
	this->pinS2 = pinS2;
	this->pinKey = pinKey;
	
	pinMode(pinS1, INPUT);
	pinMode(pinS2, INPUT);
	pinMode(pinKey, INPUT);
	
	offsetInner = 0;
	lastSState = 0;
	lastKeyState = false;
}

void Encoder::handleChangedOffsetInner() {
	if (offsetInner >= 4) {
		offsetInner -= 4;
		listener.onEncoderOffset(1);
		return;
	}
	if (offsetInner <= -4) {
		offsetInner += 4;
		listener.onEncoderOffset(-1);
		return;
	}
}

timestamp Encoder::loop() {
	
	bool keyState = !digitalRead(pinKey);
	if (keyState != lastKeyState) {
		if (keyState == true) {
			listener.onEncoderClick();
		}
		lastKeyState = keyState;
	}
	
	bool s1State = !digitalRead(pinS1);
	bool s2State = !digitalRead(pinS2);
	uint8_t sState = s1State | s2State << 1;
	if (sState != lastSState) {
		uint8_t offsetMapIndex = sState | lastSState << 2;
		offsetInner += pgm_read_byte(offsetMap+offsetMapIndex);
		handleChangedOffsetInner();
		lastSState = sState;
	}
	
	return 0;
}
