#ifndef ENCODER_H
#define ENCODER_H

#include <Thread.h>
#include <EncoderListener.h>

class Encoder: public Thread {
	
private:

	static const int8_t offsetMap[16];

	EncoderListener& listener;
	
	uint8_t pinS1;
	uint8_t pinS2;
	uint8_t pinKey;
	
	int8_t offsetInner;
	
	uint8_t lastSState;
	bool lastKeyState;
	
	void handleChangedOffsetInner();
	
protected:

    virtual timestamp loop() override;

    
public:

	Encoder(
		uint8_t pinS1,
		uint8_t pinS2,
		uint8_t pinKey,
		EncoderListener& listener_
	);
	
	
};

#endif //ENCODER_H
