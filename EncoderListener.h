#ifndef ENCODERLISTENER_H
#define ENCODERLISTENER_H

#include <Arduino.h>

class EncoderListener {

    
public:

	virtual void onEncoderClick() = 0;
	virtual void onEncoderOffset(int8_t offset) = 0;
	
	
};

#endif //ENCODERLISTENER_H
