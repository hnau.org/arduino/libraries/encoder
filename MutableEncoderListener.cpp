#include "MutableEncoderListener.h"


MutableEncoderListener::MutableEncoderListener() {
	wrappedListener = nullptr;
}

EncoderListener* MutableEncoderListener::getDelegateListener() {
	return wrappedListener;
}

void MutableEncoderListener::setListener(EncoderListener* listener) {
	wrappedListener = listener;
}
