#ifndef MUTABLEENCODERLISTENER_H
#define MUTABLEENCODERLISTENER_H

#include <DelegateEncoderListener.h>

class MutableEncoderListener: public DelegateEncoderListener {
	
private:

	EncoderListener* wrappedListener;
	
protected:

	virtual EncoderListener* getDelegateListener() override;

    
public:

	MutableEncoderListener();
	
	void setListener(EncoderListener* listener);
	
	
};

#endif //MUTABLEENCODERLISTENER_H
