#include "SimpleEncoderListener.h"

SimpleEncoderListener::SimpleEncoderListener(
	std::function<void(int8_t)> onOffset_,
	std::function<void()> onClick_
): onOffset(onOffset_), onClick(onClick_) {}


void SimpleEncoderListener::onEncoderOffset(int8_t offset) {
	onOffset(offset);
}


void SimpleEncoderListener::onEncoderClick() {
	onClick();
}
