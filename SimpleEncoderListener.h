#ifndef SIMPLEENCODERLISTENER_H
#define SIMPLEENCODERLISTENER_H

#include <EncoderListener.h>

class SimpleEncoderListener: public EncoderListener {
	
private:

	std::function<void(int8_t)> onOffset;
	std::function<void()> onClick;

    
public:

	SimpleEncoderListener(
		std::function<void(int8_t)> onOffset_,
		std::function<void()> onClick_
	);

	virtual void onEncoderOffset(int8_t offset) override;
	virtual void onEncoderClick() override;
	
	
};

#endif //SIMPLEENCODERLISTENER_H
